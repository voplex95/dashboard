import { LucilleDashboardPage } from './app.po';

describe('lucille-dashboard App', () => {
  let page: LucilleDashboardPage;

  beforeEach(() => {
    page = new LucilleDashboardPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
