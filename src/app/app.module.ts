import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app.routing';
import { AuthModule } from './auth/auth.module';

import { AppComponent } from './app.component';

import {
  HomePageComponent,
  ArticlesPageComponent,
  UsersPageComponent,
  SettingsPageComponent,
  NotFoundPageComponent
} from './core';

import {
  ArticlesOverviewComponent,
  NewArticleComponent,
  ArticleCategoriesComponent,
  ArticleCategoryService,
  ArticleService
} from './core/articles-page';

import {
  ModalComponent,
  ModalService
} from './core/shared/modal';

import { VMenuComponent } from './core/shared/vmenu';
import { CKEditorModule } from 'ng2-ckeditor';
import { RlTagInputModule as TagInputModule } from 'angular2-tag-input';


@NgModule({
  declarations: [
    AppComponent,
    VMenuComponent,
    HomePageComponent,
    ArticlesPageComponent,
    ArticlesOverviewComponent,
    NewArticleComponent,
    ArticleCategoriesComponent,
    UsersPageComponent,
    SettingsPageComponent,
    NotFoundPageComponent,
    ModalComponent
  ],
  entryComponents: [
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AuthModule,
    AppRoutingModule,
    FormsModule,
    CKEditorModule,
    TagInputModule
  ],
  providers: [
    ModalService,
    ArticleCategoryService,
    ArticleService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
