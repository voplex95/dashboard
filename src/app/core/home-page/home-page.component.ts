import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  templateUrl: './home-page.component.html'
})
export class HomePageComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    $.getScript('assets/js/jqvmap.run.js', function(){});
  }

}
