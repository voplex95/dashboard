import { Component } from '@angular/core';
import { AuthService } from '../../../auth/services';

@Component({
  selector: 'app-vmenu',
  templateUrl: './vmenu.component.html'
})
export class VMenuComponent {

  constructor(private authService: AuthService) {}

  logout(): void {
    this.authService.logout();
  }

}
