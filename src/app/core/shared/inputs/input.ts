export class Input {

    name: string;
    type: string;
    value: string;

    constructor(name: string, type: string){
        this.name = name;
        this.type = type;
    }

}