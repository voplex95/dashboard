import { Input } from './input';

export class TextInput extends Input {
    constructor(name: string){
        super(name, "text");
    }
}