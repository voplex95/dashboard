export class Page<T> {
  readonly content: T[];
  readonly first: boolean;
  readonly last: boolean;
  readonly number: number;
  readonly numberOfElements: number;
  readonly size: number;
  readonly sort: string;
  readonly totalElements: number;
  readonly totalPages: number;
}
