export class Article {
  id: string;
  createdOn: string;
  createdBy: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  title: string;
  description: string;
  categoryName: string;
  content: string;
  tagNames: string[];
}
