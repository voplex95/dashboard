import { ComponentRef, Injectable } from '@angular/core';
import { ModalComponent } from './modal.component';

@Injectable()
export class ModalService {

  constructor() {}

  display(message, inputs, onSubmit, onClose, componentResolver, viewContainerRef): ComponentRef<ModalComponent> {
    const ref = viewContainerRef.createComponent(componentResolver.resolveComponentFactory(ModalComponent));
    this.initializeModalDialogFields(ref, message, inputs, onSubmit, onClose);
    ref.changeDetectorRef.detectChanges();
    return ref.instance;
  }

  private initializeModalDialogFields(modalDialogRef, message, inputs, onSubmit, onClose) {
    const modal = modalDialogRef.instance;
    modal.ref = modalDialogRef;
    modal.message = message;
    modal.inputs = inputs;
    modal.onSubmit = onSubmit;
    modal.onClose = onClose;
  }

  destroy(modal): void {
    modal.ref.destroy();
  }

}
