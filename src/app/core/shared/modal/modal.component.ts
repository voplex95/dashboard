import { Component, ComponentRef } from '@angular/core';

import { Input } from '../../shared/inputs';

@Component({
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent {

  ref: ComponentRef<ModalComponent>;
  message: string;
  inputs: [Input]
  onSubmit: Function;
  onClose: Function;

  submit() {
    this.onSubmit(this.inputs);
    this.cancel();
  }

  cancel() {
    this.onClose(this);
  }

}
