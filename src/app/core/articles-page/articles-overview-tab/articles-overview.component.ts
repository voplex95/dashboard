import { Component, ComponentFactoryResolver, ComponentRef, OnInit, ViewContainerRef } from '@angular/core';
import { Article } from '../../shared/models';
import { ModalComponent, ModalService } from '../../shared/modal';
import { TextInput } from '../../shared/inputs';
import { Observable } from 'rxjs/Observable';
import { ArticleService } from '../article.service';

@Component({
  templateUrl: './articles-overview.component.html'
})
export class ArticlesOverviewComponent implements OnInit {

  articles: Article[];
  selectedArticles: Article[] = [];

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef,
              private modalService: ModalService,
              private articleService: ArticleService) {}

  ngOnInit() {
    this.fetchArticles();
  }

  private fetchArticles(): void {
    this.articleService.get().subscribe(data => {
      this.articles = data.content;
    });
  }

  /**
   * Adds to/removes from selected article array
   * @param {Article} article
   */
  handleRowClick(article: Article) {
    if (this.selectedArticles.includes(article)) {
      this.deselectArticle(article);
    } else {
      this.selectArticle(article);
    }
  }

  private selectArticle(article: Article) {
    this.selectedArticles.push(article);
  }

  private deselectArticle(article: Article) {
    const indexOfValue: number = this.selectedArticles.indexOf(article);
    if (indexOfValue !== -1) {
      this.selectedArticles.splice(indexOfValue, 1);
    }
  }

  /**
   * Creates new article
   */
  create(): void {
    this.invokeModal('New article', [new TextInput('Name')], this.submitNewArticle.bind(this),
      this.destroyModal.bind(this));
  }

  private invokeModal(message, inputs, onSubmit, onClose): ComponentRef<ModalComponent> {
    return this.modalService.display(
      message, inputs, onSubmit, onClose, this.componentFactoryResolver, this.viewContainerRef);
  }

  private destroyModal(modal): void {
    this.modalService.destroy(modal);
  }

  private submitNewArticle(inputs): void {
    const titleInput = inputs.filter(i => i.name === 'Title')[0];
    this.articleService.create(new Article()).subscribe(response => {
      this.fetchArticles();
      this.clearSelection();
    });
  }

  /**
   *  Updates selected article (requires single selected row)
   */
  edit(): void {
    if (this.isSingleRowSelected) {
      const titleInput = new TextInput('Title');
      titleInput.value = this.selectedArticles[0].title;
      this.invokeModal('Edit article', [titleInput], this.updateArticle.bind(this), this.destroyModal.bind(this));
    }
  }

  private updateArticle(inputs): void {
    const titleInput = inputs.filter(i => i.name === 'Title')[0];
    const selectedArticle = this.selectedArticles[0];
    selectedArticle.title = titleInput.value;
    this.articleService.update(selectedArticle).subscribe(response => {
      this.fetchArticles();
      this.clearSelection();
    });
  }

  /**
   * Removes single or multiple articles
   */
  remove(): void {
    if (this.isAnyRowSelected) {
      this.invokeModal('Delete article', null, this.removeArticle.bind(this), this.destroyModal.bind(this));
    }
  }

  private removeArticle(): void {
    const observables = this.selectedArticles.map(article => this.articleService.remove(article.id));
    Observable.forkJoin(observables).subscribe(responseArray => {
      this.fetchArticles();
      this.clearSelection();
    });
  }

  private clearSelection(): void {
    this.selectedArticles = [];
  }

  isSingleRowSelected(): boolean {
    return this.selectedArticles.length === 1;
  }

  isAnyRowSelected(): boolean {
    return this.selectedArticles.length > 0;
  }

}
