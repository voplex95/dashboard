import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { Article, Page } from '../shared/models';

@Injectable()
export class ArticleService {

  constructor(private http: AuthHttp) {}

  get(): Observable<Page<Article>> {
    return this.http.get('http://localhost:8080/articles').map(response => response.json());
  }

  create(article: Article): Observable<Response> {
    return this.http.post('http://localhost:8080/articles', article);
  }

  remove(id: string): Observable<Response> {
    return this.http.delete('http://localhost:8080/articles/' + id);
  }

  update(article: Article) {
    return this.http.put('http://localhost:8080/articles/' + article.id, article);
  }

}
