import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Page, ArticleCategory } from '../../shared/models';
import { Response } from '@angular/http';

@Injectable()
export class ArticleCategoryService {

  constructor(private http: AuthHttp) {}

  get(): Observable<Page<ArticleCategory>> {
    return this.http.get('http://localhost:8080/article-categories').map(response => response.json());
  }

  create(articleCategory: ArticleCategory): Observable<Response> {
    return this.http.post('http://localhost:8080/article-categories', articleCategory);
  }

  remove(id: string): Observable<Response> {
    return this.http.delete('http://localhost:8080/article-categories/' + id);
  }

  update(articleCategory: ArticleCategory) {
    return this.http.put('http://localhost:8080/article-categories/' + articleCategory.id, articleCategory);
  }

}
