import { Component, OnInit, ComponentFactoryResolver, ViewContainerRef, ComponentRef } from '@angular/core';
import { ModalService } from '../../shared/modal/modal.service';
import { ArticleCategoryService } from './article-category.service';

import { TextInput } from '../../shared/inputs';
import { ModalComponent } from '../../shared/modal';
import { ArticleCategory } from '../../shared/models';
import { Observable } from 'rxjs/Rx';

@Component({
  templateUrl: './article-categories.component.html',
  styleUrls: ['./article-categories.component.css']
})
export class ArticleCategoriesComponent implements OnInit {

  categories: ArticleCategory[];
  selectedCategories: ArticleCategory[] = [];

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef,
              private modalService: ModalService,
              private articleCategoryService: ArticleCategoryService) {}

  ngOnInit() {
    this.fetchCategories();
  }

  private fetchCategories(): void {
    this.articleCategoryService.get().subscribe(data => {
      this.categories = data.content;
    });
  }

  /**
   * Adds to/removes from selected category array
   * @param {ArticleCategory} category
   */
  handleRowClick(category: ArticleCategory) {
    if (this.selectedCategories.includes(category)) {
      this.deselectCategory(category);
    } else {
      this.selectCategory(category);
    }
  }

  private selectCategory(category: ArticleCategory) {
    this.selectedCategories.push(category);
  }

  private deselectCategory(category: ArticleCategory) {
    const indexOfValue: number = this.selectedCategories.indexOf(category);
    if (indexOfValue !== -1) {
        this.selectedCategories.splice(indexOfValue, 1);
    }
  }

  /**
   * Creates new article category
   */
  create(): void {
    this.invokeModal('New category', [new TextInput('Name')], this.submitNewCategory.bind(this),
      this.destroyModal.bind(this));
  }

  private invokeModal(message, inputs, onSubmit, onClose): ComponentRef<ModalComponent> {
    return this.modalService.display(
      message, inputs, onSubmit, onClose, this.componentFactoryResolver, this.viewContainerRef);
  }

  private destroyModal(modal): void {
    this.modalService.destroy(modal);
  }

  private submitNewCategory(inputs): void {
    const categoryNameInput = inputs.filter(i => i.name === 'Name')[0];
    this.articleCategoryService.create(new ArticleCategory(categoryNameInput.value)).subscribe(response => {
      this.fetchCategories();
      this.clearSelection();
    });
  }

  /**
   *  Updates selected article category (requires single selected row)
   */
  edit(): void {
    if (this.isSingleRowSelected) {
      const categoryNameInput = new TextInput('Name');
      categoryNameInput.value = this.selectedCategories[0].name;
      this.invokeModal('Edit category', [categoryNameInput], this.updateCategory.bind(this), this.destroyModal.bind(this));
    }
  }

  private updateCategory(inputs): void {
    const categoryNameInput = inputs.filter(i => i.name === 'Name')[0];
    const selectedCategory = this.selectedCategories[0];
    selectedCategory.name = categoryNameInput.value;
    this.articleCategoryService.update(selectedCategory).subscribe(response => {
      this.fetchCategories();
      this.clearSelection();
    });
  }

  /**
   * Removes single or multiple article categories
   */
  remove(): void {
    if (this.isAnyRowSelected) {
      this.invokeModal('Delete category', null, this.removeCategory.bind(this), this.destroyModal.bind(this));
    }
  }

  private removeCategory(): void {
    const observables = this.selectedCategories.map(category => this.articleCategoryService.remove(category.id));
    Observable.forkJoin(observables).subscribe(responseArray => {
      this.fetchCategories();
      this.clearSelection();
    });
  }

  private clearSelection(): void {
    this.selectedCategories = [];
  }

  isSingleRowSelected(): boolean {
    return this.selectedCategories.length === 1;
  }

  isAnyRowSelected(): boolean {
    return this.selectedCategories.length > 0;
  }

}
