import { Component, OnInit, AfterViewChecked } from '@angular/core';
import * as $ from 'jquery';

import { Article, ArticleCategory } from '../../shared/models';
import { ArticleService, ArticleCategoryService } from '../';

@Component({
  templateUrl: './new-article.component.html'
})
export class NewArticleComponent implements OnInit, AfterViewChecked {

  article: Article;
  articleCategories: ArticleCategory[];

  private _bootstrapSelectRefreshRequired: boolean;

  private static refreshBootstrapSelectElement(selector: String): void {
    const expression = '$(\'' + selector + '\').selectpicker(\'refresh\')';
    $.globalEval(expression);
  }

  constructor(private articleService: ArticleService, private articleCategoryService: ArticleCategoryService) {
    this.article = new Article();
    this.article.tagNames = [];
  }

  ngOnInit(): void {
    this.articleCategoryService.get().subscribe(categories => {
      this.articleCategories = categories.content;
      this._bootstrapSelectRefreshRequired = true;
    });
  }

  ngAfterViewChecked(): void {
    if (this._bootstrapSelectRefreshRequired) {
      NewArticleComponent.refreshBootstrapSelectElement('.selectpicker');
      this._bootstrapSelectRefreshRequired = false;
    }
  }

  submit(): void {
    this.articleService.create(this.article).subscribe(response => {}, error => alert(error));
  }

}
