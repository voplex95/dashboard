export { HomePageComponent } from './home-page/home-page.component';
export { ArticlesPageComponent } from './articles-page/articles-page.component';
export { UsersPageComponent } from './users-page/users-page.component';
export { SettingsPageComponent } from './settings-page/settings-page.component';
export { NotFoundPageComponent } from './not-found-page/not-found-page.component';