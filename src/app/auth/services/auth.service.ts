import { Injectable } from '@angular/core';
import {
  Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute,
  Params
} from '@angular/router';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import { tokenNotExpired } from 'angular2-jwt';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class AuthService implements CanActivate {

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private http: Http) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (tokenNotExpired()) {
        return true;
    }
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    return false;
  }

  login(username: string, password: string): Subscription {
    const options = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });
    return this.http.post('http://localhost:8080/auth', JSON.stringify({ username: username, password: password }), options)
      .map((response: Response) => {
          const token = response.json() && response.json().token;
          if (token) {
            localStorage.setItem('token', token);
            localStorage.setItem('currentUser', JSON.stringify({ username: username }));
            this.router.navigate([this.getReturnUrl()]);
            return true;
          } else {
            return false;
          }
      }).subscribe();
  }

  private getReturnUrl(): string {
    let returnUrl = '/';
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      returnUrl = params['returnUrl'] && params['returnUrl'];
    });
    return returnUrl;
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    this.router.navigate(['/login']);
  }

  resetPassword(email: string): Observable<boolean> {
    return this.http.post('/api/reset', JSON.stringify({ email: email }))
      .map((response: Response) => {
          return response.ok;
      });
  }
}
