import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LoginPageComponent } from './components';

@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: 'login', component: LoginPageComponent }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AuthRoutingModule {}