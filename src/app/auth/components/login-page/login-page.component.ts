import { Component, AfterViewInit } from '@angular/core';
import { AuthService } from '../../services';
import * as $ from 'jquery';

@Component({
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements AfterViewInit {

  email: string;
  password: string;

  constructor(private authService: AuthService) {}

  ngAfterViewInit() {
    $.getScript('assets/js/login.js', function(){});
  }

  login(): void {
    this.authService.login(this.email, this.password);
  }

  resetPassword(): void {
    this.authService.resetPassword(this.email);
  }

}
