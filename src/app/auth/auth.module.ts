import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AuthRoutingModule } from './auth.routing';
import { FormsModule } from '@angular/forms';

import { Http, RequestOptions } from '@angular/http';
import { AuthHttp, AuthConfig } from 'angular2-jwt';

import { AuthService } from './services';
import { LoginPageComponent } from './components';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig(), http, options);
}

@NgModule({
  imports: [
    HttpModule,
    AuthRoutingModule,
    FormsModule
  ],
  providers: [
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    },
    AuthService
  ],
  declarations: [
    LoginPageComponent
  ]
})
export class AuthModule { }
