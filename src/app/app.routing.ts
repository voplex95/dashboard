import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuthService } from './auth/services';

import {
  HomePageComponent,
  ArticlesPageComponent,
  UsersPageComponent,
  SettingsPageComponent,
  NotFoundPageComponent
} from './core';

import {
  ArticlesOverviewComponent,
  NewArticleComponent,
  ArticleCategoriesComponent
} from './core/articles-page';

@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: '', component: HomePageComponent, canActivate: [AuthService] },
            { path: 'articles', component: ArticlesPageComponent, canActivate: [AuthService], children: [
                { path: '', redirectTo: 'overview', pathMatch: 'full' },
                { path: 'overview', component: ArticlesOverviewComponent },
                { path: 'compose', component: NewArticleComponent },
                { path: 'categories', component: ArticleCategoriesComponent }
            ]},
            { path: 'users', component: UsersPageComponent, canActivate: [AuthService] },
            { path: 'settings', component: SettingsPageComponent, canActivate: [AuthService] },
            { path: '**', component: NotFoundPageComponent }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}
